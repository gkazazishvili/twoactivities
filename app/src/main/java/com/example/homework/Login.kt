package com.example.homework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.homework.databinding.ActivityLoginBinding
import com.example.homework.databinding.ActivityMainBinding

class Login : AppCompatActivity() {


    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

    }

    private fun init(){
        actionBarTitle()

        val username = intent.getStringExtra("username")
        val email = intent.getStringExtra("email")
        val password = intent.getStringExtra("password")

        binding.emailTextView.text = email.toString()
        binding.usernameTextView.text = username.toString()
        binding.passwordTextView.text = password.toString()



    }

    private fun actionBarTitle(){
        val actionBar = supportActionBar
        actionBar!!.title = "Login Page"
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
}