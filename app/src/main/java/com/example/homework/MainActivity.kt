package com.example.homework

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.homework.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        actionBarTitle()
        listeners()


    }


    private fun listeners() {
        binding.login.setOnClickListener {
            if (validateUsername() && validateEmail() && validatePassword()) {
                val intent =
                    Intent(this, Login::class.java).putExtra("email", binding.email.text.toString())
                        .putExtra("username", binding.userName.text.toString())
                        .putExtra("password", binding.password.text.toString())

                startActivity(intent)
            }
            if (!validateUsername()) {
                binding.usernameMessage.text = "Username must consists of 6 to 30 characters"
            } else{
                binding.usernameMessage.text = ""
            }
            if (!validatePassword()) {
                binding.passwordMessage.text = "Password must consists of 8 to 30 characters"
            } else {
                binding.passwordMessage.text = ""
            }
            if (!validateEmail()) {
                binding.emailMessage.text = "Email is not valid"
            } else{
                binding.emailMessage.text = ""
            }
        }
    }


    private fun actionBarTitle() {
        val actionBar = supportActionBar
        actionBar!!.title = "Home Page"
    }

    private fun validateEmail(): Boolean {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        return binding.email.text.matches(emailPattern.toRegex())
    }

    private fun validateUsername(): Boolean {
        val usernamePattern = "^[A-Za-z]\\w{5,29}$"
        return binding.userName.text.matches((usernamePattern.toRegex()))
    }

    private fun validatePassword(): Boolean {
        val passwordPattern = "^[A-Za-z]\\w{8,29}\$"
        return binding.password.text.matches(passwordPattern.toRegex())
    }

}